import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { GetAllProductsAction, GetSelectedProductsAction, SearchProductsAction } from 'src/app/ngrx/products.actions';

@Component({
  selector: 'app-products-nav-bar',
  templateUrl: './products-nav-bar.component.html',
  styleUrls: ['./products-nav-bar.component.css']
})
export class ProductsNavBarComponent implements OnInit {

  constructor(private store: Store<any>, private router:Router) { }

  ngOnInit(): void {
  }

  onGetAllProducts(): void {
    this.store.dispatch(new GetAllProductsAction({}))
  }

  onGetSelectedProduct(): void {
    this.store.dispatch(new GetSelectedProductsAction({}))
  }

  onSearch(dataForm: any): void {
    this.store.dispatch(new SearchProductsAction(dataForm.keyword))
  }

  onNewProduct() {
    this.router.navigateByUrl('/newProduct')
  }

}
