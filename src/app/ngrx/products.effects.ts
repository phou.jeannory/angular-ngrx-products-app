import { Injectable } from '@angular/core';
import { ProductService } from '../services/product.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { Action } from '@ngrx/store';
import {
  DeleteProductActionError,
  DeletetProductActionSuccess,
  GetAllProductsActionError,
  GetAllProductsActionSuccess,
  GetSelectedProductsActionError,
  GetSelectedProductsActionSuccess,
  NewProductActionSuccess,
  ProductsActions,
  ProductsActionsTypes,
  SaveProductActionError,
  SaveProductActionSuccess,
  SearchProductsActionError,
  SearchProductsActionSuccess,
  SelectProductActionError,
  SelectProductActionSuccess
} from './products.actions';
import { catchError, map, mergeMap } from 'rxjs/operators';

@Injectable()
export class ProductsEffects {

  constructor(private productService: ProductService, private effectActions: Actions) {
  }

  /* Get All products */
  getAllProductsEffect: Observable<ProductsActions> = createEffect(
    () => this.effectActions.pipe(
      ofType(ProductsActionsTypes.GET_ALL_PRODUCTS),
      mergeMap((action: ProductsActions) => {
        return this.productService.getProducts()
          .pipe(
            map((products) => new GetAllProductsActionSuccess(products)),
            catchError((err) => of(new GetAllProductsActionError(err.message)))
          )
      })
    )
  );

  /* Get Selected products */
  getSelectedProductsEffect: Observable<ProductsActions> = createEffect(
    () => this.effectActions.pipe(
      ofType(ProductsActionsTypes.GET_SELECTED_PRODUCTS),
      mergeMap((action: ProductsActions) => {
        return this.productService.getSelectedProducts()
          .pipe(
            map((products) => new GetSelectedProductsActionSuccess(products)),
            catchError((err) => of(new GetSelectedProductsActionError(err.message)))
          )
      })
    )
  );

  /* Search products */
  searchProductsEffect: Observable<ProductsActions> = createEffect(
    () => this.effectActions.pipe(
      ofType(ProductsActionsTypes.SEARCH_PRODUCTS),
      mergeMap((action: ProductsActions) => {
        return this.productService.searchProducts(action.payload)
          .pipe(
            map((products) => new SearchProductsActionSuccess(products)),
            catchError((err) => of(new SearchProductsActionError(err.message)))
          )
      })
    )
  );

  /* Select product */
  SelectProductsEffect: Observable<ProductsActions> = createEffect(
    () => this.effectActions.pipe(
      ofType(ProductsActionsTypes.SELECT_PRODUCT),
      mergeMap((action: ProductsActions) => {
        return this.productService.setSelected(action.payload)
          .pipe(
            map((product) => new SelectProductActionSuccess(product)),
            catchError((err) => of(new SelectProductActionError(err.message)))
          )
      })
    )
  );

  /* Delete Product Effect*/
  DeleteProductsEffect: Observable<ProductsActions> = createEffect(
    () => this.effectActions.pipe(
      ofType(ProductsActionsTypes.DELETE_PRODUCT),
      mergeMap((action: ProductsActions) => {
        return this.productService.delete(action.payload.id)
          .pipe(
            map(() => new DeletetProductActionSuccess(action.payload)),
            catchError((err) => of(new DeleteProductActionError(err.message)))
          )
      })
    )
  );

  /* New Product Effect*/
  NewProductsEffect: Observable<ProductsActions> = createEffect(
    () => this.effectActions.pipe(
      ofType(ProductsActionsTypes.NEW_PRODUCT),
      map((action: ProductsActions) => {
        return new NewProductActionSuccess({});
      })
    )
  );

  /* Save Product Effect*/
  SaveProductsEffect: Observable<ProductsActions> = createEffect(
    () => this.effectActions.pipe(
      ofType(ProductsActionsTypes.SAVE_PRODUCT),
      mergeMap((action: ProductsActions) => {
        return this.productService.save(action.payload)
          .pipe(
            map((product) => new SaveProductActionSuccess(product)),
            catchError((err) => of(new SaveProductActionError(err.message)))
          )
      })
    )
  );

}
