# angular-ngrx-products-app

npm run start

# part-1 installation

npm install --save @ngrx/store
npm install --save @ngrx/effects
npm install --save @ngrx/store-devtools

# other version

npm install --save @ngrx/store@^10.0.0
npm install --save @ngrx/effects@^10.0.0
npm install --save @ngrx/store-devtools@^10.0.0
